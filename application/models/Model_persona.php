<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class model_persona extends CI_Model {

    protected $db;

    /**
     * Construct
     */
    function __construct() {
        parent::__construct();
        $this->sql = $this->load->database('default', TRUE);
    }

    function verPersona($id='')
    {
    	if($id == '') {
    		$this->sql->select('*');
    		$this->sql->from('persona');

    		$result = $this->sql->get();
        
	        if ($result->num_rows() > 0) {
	            return $result->result();
	        } else {
	            return false;
	        }
    	}else{
    		$this->sql->select('*');
    		$this->sql->from('persona');
    		$this->sql->where('id', $id);

    		$result = $this->sql->get();
        
	        if ($result->num_rows() > 0) {
	            return $result->result();
	        } else {
	            return false;
	        }

    	}
    }

    function crearPersona($data)
    {
    	$this->sql->insert('persona', $data);
    }

    function modificarPersona($data, $id)
    {
    	$this->sql->where('id', $id);
        $this->sql->update('persona', $data); 
    }

    function eliminaPersona($id)
    {
    	$this->sql->where('id', $id);
        $this->sql->delete('persona');    	
    }

    function buscar_telefonos($telefono)
    {
    	$this->sql->select('telefono');
    	$this->sql->from('persona');
    	$this->sql->where('telefono', $telefono);

    	$result = $this->sql->get();

    	if ($result->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    function buscar_telefonos_antiguo($id)
    {
    	$this->sql->select('telefono');
    	$this->sql->from('persona');
    	$this->sql->where('id', $id);

    	$result = $this->sql->get();

    	if ($result->num_rows() > 0) {
            return $result->result_array()[0]['telefono'];
        } else {
            return false;
        }
    }

}
