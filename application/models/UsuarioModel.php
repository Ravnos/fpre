<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class usuarioModel extends CI_Model {

    protected $db;

    /**
     * Construct
     */
    function __construct() {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    public function lista_usuario($username) {
        $this->db->select('*');
        $this->db->from('presupuesto.usuarios');
        $this->db->where('mail', $username);
        $result = $this->db->get();
        
        if ($result->num_rows() == 1) {
            return $result->result()[0];

        } else {
            return false;
        }
    }

    public function lista_usuarios_disponibles() {
        $this->db->select('*');
        $this->db->from('presupuesto.usuarios');

        $result = $this->db->get();
        if ($result->num_rows() == 0) {
            return false;
        } else {
            return $result->result();
        }
    }

    function agrega_usuario($datos) {
        $this->db->insert('presupuesto.usuarios', $datos);
    }

    function elimina_usuario($id) {
        $this->db->where('id', $id);
        $this->db->update('presupuesto.usuarios');
    }

    function edita_usuario($datos, $id) {
        $this->db->where('id', $id);
        $this->db->update('presupuesto.usuarios', $datos);
    }

}
