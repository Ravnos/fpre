<?php

function login_required($index = FALSE) {
    $CI = & get_instance();
    $user = $CI->session->userdata('fpre_user_id');
    if (!isset($user)) {
        redirect(base_url() . 'log');
        exit();
    }
    if ($index && isset($user)) {
        redirect(base_url() . 'home');
    }
}

