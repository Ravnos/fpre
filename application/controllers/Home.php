<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        // $this->load->model('documentos_model');
    }

    public function index() 
    {
        login_required();
        $this->load->view('admin/head');
        $this->load->view('admin/content');
        $this->load->view('admin/foot');
    }

}
