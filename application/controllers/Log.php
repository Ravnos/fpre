<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class log extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('usuarioModel'));
    }

    function index() {
        $user = $this->session->userdata('fpre_user_id');
        if ($user != '') {
            redirect(base_url() . 'home/');
        } else {

            $this->load->view('log/login');
        }
    }

    function _validar_campos($user, $pass) {
        if ($user == '') {
            $respuesta = array('access' => false, 'mensaje' => 'Debe ingresar el nombre de usuario');
            echo json_encode($respuesta);
            exit();
        }
        if ($pass == '') {
            $respuesta = array('access' => false, 'mensaje' => 'Debe ingresar La password del usuario');
            echo json_encode($respuesta);
            exit();
        }
    }

    function _valida_usuario($user, $pass_input) {

        $resp = $this->usuarioModel->lista_usuario($user);

        if ($resp == false) {
            $respuesta = array('access' => false, 'mensaje' => 'Usuario no existe');
            echo json_encode($respuesta);
            exit();
        } else {
            $this->_valida_password(md5($pass_input), $resp->password, $resp);
        }
    }

    function _valida_password($pass_input, $pass_db, $datos_usuario) {
        if ($pass_input != $pass_db) {
            $respuesta = array('access' => false, 'mensaje' => 'Clave invalida, intente nuevamente');
            echo json_encode($respuesta);
            exit();
        } else {
            $respuesta = array('access' => true, 'mensaje' => 'Login correcto');
            $session = array(
                'fpre_user_id' => $datos_usuario->id,
                'fpre_user_nombre' => $datos_usuario->nombre,
                'fpre_user_mail' => $datos_usuario->mail,
                'fpre_user_privilegios' => $datos_usuario->privilegios,
            );
            //crear datos session
            $this->session->set_userdata($session);
            echo json_encode($respuesta);
            exit();
        }
    }

    function in() {
        $user = $this->input->post('usuerMail');
        $pass = $this->input->post('userPassword');

        $this->_validar_campos($user, $pass);
        $this->_valida_usuario($user, $pass);
        
       redirect(base_url() . 'home');
    }

    function out() {
        $datos_session = array(
            'fpre_user_id',
            'fpre_user_nombre',
            'fpre_user_mail',
            'fpre_user_privilegios',
        );
        $this->session->unset_userdata($datos_session);
        session_destroy();
        redirect(base_url() . 'log');
    }

}
