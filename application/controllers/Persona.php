<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Persona extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->load->model('model_persona');
    }

    public function index() 
    {
        login_required();
        $this->load->view('admin/head');
        $this->load->view('personas/mod_personas');
    }

    function mostrarPersonas()
    {
        if (!$this->input->post('id')) {

            $data['persona'] = $this->model_persona->verPersona();
            $this->load->view('personas/tbodyVerPersona', $data);

        }else{

            $id = $this->input->post('id');

            $data['persona'] = $this->model_persona->verPersona($id);
            $this->load->view('personas/tbodyVerPersona', $data);
        }
    }

    function agregar_persona()
    {
        $personaNueva = $this->input->post();

        if ($personaNueva['nombre'] == '') {
           $mensaje['mensaje'] = 'nombre';
           echo $mensaje['mensaje'];
           exit();
        }else if ($personaNueva['telefono'] == '') {
           $mensaje['mensaje'] = 'telefono';
           echo $mensaje['mensaje'];
           exit();
        }else{
            $fonos = $this->model_persona->buscar_telefonos($personaNueva['telefono']);        

            if ($fonos == false) {
                $mensaje['mensaje'] = 'repetido';
                echo $mensaje['mensaje'];
                exit();
            }else{
                $this->model_persona->crearPersona($personaNueva);
                unset($_POST['id']);
                $this->mostrarPersonas();
            }
        }

    }

    function editar_persona()
    {
        $data = $this->input->post();
        $personaeditar = array(
                                'nombre' => $data['nombre'], 
                                'apellidos' => $data['apellidos'], 
                                'correo' => $data['correo'], 
                                'telefono' => $data['telefono'], 
                              );
        $id = $data['id'];

        if ($personaeditar['nombre'] == '') {
           $mensaje['mensaje'] = 'nombre';
           echo $mensaje['mensaje'];
           exit();
        }else if ($personaeditar['telefono'] == '') {
           $mensaje['mensaje'] = 'telefono';
           echo $mensaje['mensaje'];
           exit();
        }else{
            $fonoAntiguo = $this->model_persona->buscar_telefonos_antiguo($id);

            if ($fonoAntiguo !== $personaeditar['telefono'] ) {
                
                $fonos = $this->model_persona->buscar_telefonos();
            
                foreach ($fonos as $key => $value) {
                    $telefonos[$key] = $value['telefono'];
                }

                if (in_array($personaeditar['telefono'], $telefonos)) {
                    $mensaje['mensaje'] = 'repetido';
                    echo $mensaje['mensaje'];
                    exit();
                }else{
                    $this->model_persona->modificarPersona($personaeditar, $id);
                    unset($_POST['id']);
                    $this->mostrarPersonas();
                }
            }else{
                $this->model_persona->modificarPersona($personaeditar, $id);
                unset($_POST['id']);
                $this->mostrarPersonas();  
            }
            
        }    
    }

    function eliminar_perosna()
    {
        $personaElimiar = $this->input->post();

        $this->model_persona->eliminaPersona($personaElimiar['id']);
        unset($_POST['id']);
        $this->mostrarPersonas();
    }



}
