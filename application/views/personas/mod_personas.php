<div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="">Personas
            <small></small>
        </h1>
        
    </section>

    <section class="content ">
        <div class="contenido ">
            <div id="exTab1" class="col-xs-12"> 
                <div class="box">
                    <div class="box-header">
                        <button class="btn btn-success" type="button" data-toggle="collapse" data-target="#mostrarAgregarCedentes" aria-expanded="false" aria-controls="mostrarAgregarCedentes">
                          Agregar
                        </button>
                        <div class="collapse" id="mostrarAgregarCedentes">
                          <div class="well">
                            <form class="form-inline" id="frmAgregarPersona">
                              <div class="form-group">
                                <label for="inpAgregarNombre">Nombre</label>
                                <input type="text" class="form-control" id="inpAgregarNombre" placeholder=" Nombre ">
                              </div>
                              <div class="form-group">
                                <label for="inpAgregarApellido">Apellidos</label>
                                <input type="text" class="form-control" id="inpAgregarApellido" placeholder=" Apellido ">
                              </div>
                              <div class="form-group">
                                <label for="inpAgregarCorreo">Correo</label>
                                <input type="text" class="form-control" id="inpAgregarCorreo" placeholder=" Correo ">
                              </div>
                              <div class="form-group">
                                <label for="inpAgregarTelefono">Telefono</label>
                                <input type="text" class="form-control" id="inpAgregarTelefono" placeholder=" Telefono ">
                              </div>
                              <button type="submit" class="btn btn-default" id="btnAgregarPersona"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
                            </form>
                          </div>
                        </div>
                    </div>
                    <div class="box-body">

                         <table class="table table-striped table-hover">
                            <tr class="bg-primary">
                                <th class="bg-primary">ID</th>
                                <th class="bg-primary">Nombre</th>
                                <th class="bg-primary">Apellidos</th>
                                <th class="bg-primary">Correo</th>
                                <th class="bg-primary">Telefono</th>
                                                                      
                                <th class="bg-primary"><span class="glyphicon glyphicon-cog"></span></th>
                            </tr> 
                            <tbody id="tbodyVerPersona">
                                <!-- aca se muestran los datos  -->

                            </tbody>                          
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalPersona">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modificar</h4>
      </div>
      <div class="modal-body">
        <form class="" id="frmModificarPersona">
            <input type="text" class="hidden" name="" id="inpModificarId">
          <div class="form-group">
            <label for="inpModificarNombre">Nombre</label>
            <input type="text" class="form-control" id="inpModificarNombre">
          </div>
          <div class="form-group">
            <label for="inpModificarapellido">Apellidos</label>
            <input type="text" class="form-control" id="inpModificarapellido">
          </div>
          <div class="form-group">
            <label for="inpModificarCorreo">Correo</label>
            <input type="text" class="form-control" id="inpModificarCorreo">
          </div>
          <div class="form-group">
            <label for="inpModificarTelefono">Telefono</label>
            <input type="text" class="form-control" id="inpModificarTelefono">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btnModificarPersona">Guardar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


 <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1 (Alpha)
      </div>
      <strong>Copyright &copy; 2018 </strong> 
    </div>
    <!-- /.container -->
  </footer>
  <div class="clearfix"></div>
</div>
<!-- ./wrapper -->

<script type="text/javascript" src="<?= base_url() ?>AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/dist/js/adminlte.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/dist/js/demo.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/sweetalert.min.js" ></script>
<script type="text/javascript" src="<?= base_url() ?>js/ckeditor-classic/ckeditor.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/ckeditor-classic/translations/es.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    
        $("#tbodyVerPersona").load("mostrarPersonas");
  });


    $(document).on('click', '#btnAgregarPersona', function(e){ 
        
        e.preventDefault();

        var nombre  = $('#inpAgregarNombre').val();
        var apellidos  = $('#inpAgregarApellido').val();
        var correo  = $('#inpAgregarCorreo').val();
        var telefono  = $('#inpAgregarTelefono').val();

        $.ajax({
            data: {
                'nombre'   : nombre.trim(),
                'apellidos'   : apellidos.trim(),
                'correo'   : correo.trim(),
                'telefono'   : telefono.trim(),
            },
            type: "POST",
            url: "agregar_persona",
            dataType: 'html',
            success: function (data) {
                console.log(data);
                if (data == 'nombre') {
                  swal("Favor ingrese un nombre");
                }else if(data == 'telefono'){
                  swal("Favor ingrese un Teleono");
                }else if(data == 'repetido'){
                  swal("Telefono Repetido");
                }else{
                  $('#tbodyVerPersona').html(data);
                  $('#frmAgregarPersona').each(function() {
                      this.reset();
                  });
                  swal("Agregado");
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                swal("Ocurrio un arror");
            }
        });        
    });

  $(document).on('click', '#btnEditaPersona', function(){

        var myModal = $('#modalPersona');

        var id      = $(this).closest('tr').find('td.tdId').html();
        var nombre  = $(this).closest('tr').find('td.tdNombre').html();
        var apellido    = $(this).closest('tr').find('td.tdApellidos').html();
        var correo = $(this).closest('tr').find('td.tdCorreo').html();
        var telefono  = $(this).closest('tr').find('td.tdtelefono').html();
                
        $('#inpModificarId', myModal).val(id);
        $('#inpModificarNombre', myModal).val(nombre);
        $('#inpModificarapellido', myModal).val(apellido);
        $('#inpModificarCorreo', myModal).val(correo);
        $('#inpModificarTelefono', myModal).val(telefono);
        
        // console.log(estado);
        myModal.modal({show: true});

    });

  $(document).on('click', '#btnModificarPersona', function(e){ 
        
        e.preventDefault();

        var id  = $('#inpModificarId').val();
        var nombre  = $('#inpModificarNombre').val();
        var apellidos  = $('#inpModificarapellido').val();
        var correo  = $('#inpModificarCorreo').val();
        var telefono  = $('#inpModificarTelefono').val();

        $.ajax({
            data: {
                'id'      : id.trim(),
                'nombre'   : nombre.trim(),
                'apellidos'   : apellidos.trim(),
                'correo'   : correo.trim(),
                'telefono'   : telefono.trim(),
            },
            type: "POST",
            url: "editar_persona",
            dataType: 'html',
            success: function (data) {
                console.log(data);
                if (data == 'nombre') {
                  swal("Favor ingrese un nombre");
                }else if(data == 'telefono'){
                  swal("Favor ingrese un Teleono");
                }else if(data == 'repetido'){
                  swal("Telefono Repetido");
                }else{
                  $('#tbodyVerPersona').html(data);
                  $('#frmModificarPersona').each(function() {
                      this.reset();
                  });
                  $('#modalPersona').modal('hide');
                  swal("Modificado");
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                swal("Ocurrio un arror");
            }
        });        
    });

  $(document).on('click', '#btnEliminaPersona', function(e){ 
        
        e.preventDefault();

        var id  = $(this).closest('tr').find('td.tdId').html();

        $.ajax({
            data: {
                'id': id,
            },
            type: "POST",
            url: "eliminar_perosna",
            dataType: 'html',
            success: function (data) {
              $('#tbodyVerPersona').html(data);
              swal("Eliminado");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                swal("Ocurrio un arror");
            }
        });        
    });
  

</script>
</body>
</html>
