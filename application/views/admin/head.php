<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Personas | mantenedor</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url() ?>AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>AdminLTE/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>AdminLTE/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>css/sweetalert.css" rel="stylesheet" type="text/css"/>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?= base_url() ?>" class="navbar-brand"><b>Mantenedor </b>Personas</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <?php if(isset($_SESSION)){ ?>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active">
              <a href="<?= base_url() ?>persona/">Mantenedor <span class="sr-only">(current)</span></a>
            </li>
            <li class="dropdown hidden">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informes <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Sin informes aun</a></li>
              </ul>
            </li>
            <li class="dropdown hidden">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mantenedores <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Usuarios</a></li>
              </ul>
            </li>
          </ul>
          <form class="navbar-form navbar-left hidden" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form>
        </div>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="hidden-xs"><?= $_SESSION['fpre_user_nombre'] ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <p>
                    <?= $_SESSION['fpre_user_nombre'] ?>
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body hidden">
                  <div class="row">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left hidden">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?= base_url() ?>log/out" class="btn btn-default btn-flat">Cerrar Sesion</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <?php } ?>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>