 <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1 (Alpha)
      </div>
      <strong>Copyright &copy; 2018 </strong> 
    </div>
    <!-- /.container -->
  </footer>
  <div class="clearfix"></div>
</div>
<!-- ./wrapper -->

<script type="text/javascript" src="<?= base_url() ?>AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/dist/js/adminlte.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>AdminLTE/dist/js/demo.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/sweetalert.min.js" ></script>
<script type="text/javascript" src="<?= base_url() ?>js/ckeditor-classic/ckeditor.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/ckeditor-classic/translations/es.js"></script>

<script type="text/javascript">
  	$(document).ready(function () {

  		
  		ClassicEditor
    	.create( document.querySelector( '#encabezado' ),{
    		toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
    		language: 'es',
    		// image : imageConfig,
    	} )
	    .then( editor => {
	        console.log( 'Editor was initialized', editor );
	    } )
	    .catch( err => {
	        console.error( err.stack );
	    } );

	    ClassicEditor
    	.create( document.querySelector( '#pie_pagina' ),{
    		toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
    		language: 'es',
    		// image : imageConfig,
    	} )
	    .then( editor => {
	        console.log( 'Editor was initialized', editor );
	    } )
	    .catch( err => {
	        console.error( err.stack );
	    } );
  	
	});
	

</script>
</body>
</html>

