<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>F.PRE | Sistema Generacion Presupestos</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="AdminLTE/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="AdminLTE/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="AdminLTE/index2.html" class="navbar-brand"><b>Sistema </b>F.PRE</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <?php if(!$_SESSION){ ?>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Link</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li class="divider"></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form>
        </div>
        <?php } ?>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header hidden ">
        <h1>
          Top Navigation
          <small>Example 2.0</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Layout</a></li>
          <li class="active">Top Navigation</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content col-sm-6 ">
        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Iniciar Sesion</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" id="form_login" action="log/in">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-9">
                    <input type="email" class="form-control" id="usuerMail" name="usuerMail" placeholder="Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Contraseña</label>

                  <div class="col-sm-9">
                    <input type="password" class="form-control" id="userPassword" name="userPassword" placeholder="Password">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <label id="login_mensaje"></label>
                <button type="submit" class="btn btn-primary pull-right">Entrar</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1 (Alpha)
      </div>
      <strong>Copyright &copy; 2018 </strong> 
    </div>
    <!-- /.container -->
  </footer>
  <div class="clearfix"></div>
</div>
<!-- ./wrapper -->

<script type="text/javascript" src="AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<script type="text/javascript" src="AdminLTE/dist/js/adminlte.min.js"></script>
<script type="text/javascript" src="AdminLTE/dist/js/demo.js"></script>
<script type="text/javascript" src="js/sweetalert.min.js" ></script>

<script type="text/javascript">
  
  $('#form_login').submit(function( event ) {
    event.preventDefault();

    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      dataType: "json",
      data: $(this).serialize()
    }).done(function (data) {
      console.log(data);
      if (data.access === true) {
          location.href = 'home/';
      } else if (data.access === false) {
          $('#login_mensaje').html(data.mensaje);
      }
    }).fail(function () {
       swal('', 'Lo sentimos ha ocurrido un error inesperado en la validación de acceso', 'error');
    }).always(function () {
      console.log('siempre');
    });
});

</script>
</body>
</html>
